#include <stdio.h>

typedef char* Elem_t;

const char separation[] = "\n------------------------------------------------------------------------------------\n";
const char logName[] = "listLog.log";

enum treeErr {
    OK = 0, EMPTY = 1, NEGATIVE_SIZE = 2, EXTERNAL_INTRUSION = 3
};

enum printOrder{
	preOrder = -1, inOrder = 0, postOrder = 1
};

enum childSide {
	leftSide = 0, rightSide = 1
};

extern const char *errMsg[];

struct TreeNode{
	Elem_t val;
	TreeNode *parent;
	TreeNode *left;
	TreeNode *right;
};

struct Tree{
	TreeNode *root;
	treeErr err;
	int vertQty;
};

TreeNode *findNode( Tree *this_, Elem_t val );

void clearLog();

void msgToLog( const char *msg );

void treeInit( Tree *tree );

void destructTree( Tree *tree );

treeErr treeOk( Tree *tree );

void printTree( const Tree *tree, printOrder order = preOrder );

void addChild( Tree* this_, Elem_t val, TreeNode *parent, childSide side = leftSide );

void addSubTree( Tree *parentTree, TreeNode *parent, Tree *subTree, childSide side = leftSide );

void deleteChild( Tree *this_, TreeNode *child );

void deleteSubTree( Tree *this_, Tree *subTree = nullptr, TreeNode *node = nullptr );

void tellMeEverythingYouKnow( Tree *tree );

void showTree( Tree *this_ );

void writeTree( char *fname, Tree *workTree );

void readTree( char *fname, Tree *workTree );
