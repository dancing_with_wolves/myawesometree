run: build
	./Akinator.e
build: Akinator.o tree.o
	clang++ Akinator.o tree.o -o Akinator.e
Akinator.o: Akinator.cpp
	clang++ -c Akinator.cpp -o Akinator.o
tree.o: tree.cpp
	clang++ -c tree.cpp
clean:
	rm *.o
	rm *.e
	rm *.log
