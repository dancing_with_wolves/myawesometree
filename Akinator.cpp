#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "tree.h"

bool questionCorrect( char *buf ){
	return true;
}

int countFileSize ( char *fileName ){
    assert( fileName );
    FILE *in = (FILE*) fopen ( fileName, "r" );
    assert( in );
    if ( !in ) return -1;
    int fileSize = 0;
    fseek( in, 0, SEEK_END );
    fileSize = ftell( in );
    fclose( in );
    return fileSize;
}

Tree *initAkinatorData( char *fname ){
	assert( fname );

	FILE *in = fopen( fname, "w" );
	Tree *tree = ( Tree* ) calloc ( 1, sizeof ( Tree ) );
	treeInit( tree );
	readTree( "data.tree", tree );

	fclose( in );
	return tree;
}

void printDifference( Tree *this_, TreeNode *desc1, TreeNode *desc2 ){
	
}

void printWordDescription( Tree *this_, TreeNode *descr ){
	assert( this_ );
	assert( descr );
	if( descr == this_-> root ){
		return;
	}

	printWordDescription( this_, descr -> parent );

	if( descr == descr -> parent -> left ){
		printf( "НЕ %s; ", descr -> parent -> val );
	} else {
		printf( "%s; ", descr -> parent -> val );
	}

}

void addNewWord( Tree *workTree, TreeNode *parent ){

	char question[128] = "";
	printf( "Введите отличающий ваш загаданный объект признак:\n");
	scanf( "%*c");
	scanf( "%[^\n]", question );

	while( !questionCorrect( question ) ){
		printf( "Ошибка, запрещённое слово НЕ! Введите существенный признак!\n" );
		scanf( "%*c");
		scanf( "%[^\n]", question );
	}

	char ans[3] = "";
	printf( "Введите верный для вашего объекта ответ на этот вопрос:\n");

	scanf( "%s", ans );
	while( ans[0] != 'N' && ans[0] != 'n' && ans[0] != 'Y' && ans[0] != 'y' ) {
		printf( "Некорректный ответ! Ответом являются буквы Y, y, N, n\n");
		printf( "Введите верный для вашего объекта ответ на вопрос:\n");
		scanf( "%s", ans );
	}

	char buf[64] = "";

	printf( "Введите загаданный объект:\n");
	scanf( "%*c");
	scanf( "%[^\n]", buf );
	TreeNode *found = findNode( workTree, buf );
	while( found ){
		printf( "Такой объект уже есть в базе и имеет следующее описание:\n");
		printWordDescription( workTree, found );
		printf( "Введите загаданный объект:\n");
		scanf( "%*c");
		scanf( "%[^\n]", buf );
		found = findNode( workTree, buf );
	}
	char *object = strdup( buf );

	if( ans[0] == 'y' || ans[0] == 'Y' ){
		addChild( workTree, parent -> val, parent, leftSide );
		addChild( workTree, object, parent, rightSide );
	} else {
		addChild( workTree, object, parent, leftSide );
		addChild( workTree, parent -> val, parent, rightSide );
	}
	parent -> val = strdup( question );
}

void hello(){
	printf( "Привет! Я акинатор. Надеюсь, ты уже задумал слово, и я постараюсь его угадать.\n");
}

void submitChanges( char *fileTo, char *fileFrom ){
	char buf[1024] = "";
	FILE *in = fopen( fileFrom, "rb" );
	FILE *out = fopen( fileTo, "wb" );

	size_t inSize = countFileSize( fileFrom );
	fread( buf, sizeof(char), inSize, in );
	fwrite( buf, sizeof(char), inSize, out );

	fclose( in );
	fclose( out );
}

int main(){
	clearLog();
	hello();
	Tree* workTree = initAkinatorData( "AkinatorData.txt" );

	showTree( workTree );

	bool cont = true;
	char ans[128] = "";
	TreeNode *cur = workTree -> root;

	//цикл акинатора
	while( cont ){
		printf( "%s?\n", cur -> val );
		scanf( "%s", ans );
		if( ans[0] == 'y' || ans[0] == 'Y' ) {
			cur = cur -> right;
		} else {
			cur = cur -> left;
		}
		if( !cur -> left && !cur -> right ) cont = false;
	}
	printf( "Ваше слово -- %s?\n", cur -> val );
	scanf( "%s", ans );

	if( ans[0] == 'y' || ans[0] == 'Y' ) {
		printf( "Отлично!\n");
	} else {
		addNewWord( workTree, cur );
		writeTree( "reData.tree", workTree );
		submitChanges( "data.tree", "reData.tree" );
	}
	showTree( workTree );

}
